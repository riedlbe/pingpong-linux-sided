1. add tap-device: ip tap add tap0 mode tap
2. set tap0 up: ip link set tap0 up
3. compile and start RIOT sided application with make and sudo make term 
4. compile and start Linux-sided application
5. in an additional terminal in linux: ifconfig, read out assigned ipv6 link-local address of tap0 addr
6. in RIOT application: pingpong addr

Linux and RIOT should be playing pingpong now!