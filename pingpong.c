#include <sys/ioctl.h>
#include <net/if.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <netinet/in.h>

#define CLEAR(x) memset(&(x), 0x00, sizeof(x))

#define IFNAME "tap0"
#define ANSWER "PONG"
#define REQUEST "PING"
#define REQUEST "PING"

#define BUFLEN 1600
#define PORT 8808

int get_ipv6_from_if(char* if_name, struct sockaddr_in6 *addr){
    struct ifaddrs *ifaddr, *  ifa;

    if (getifaddrs(&ifaddr) == -1) {
        perror("Error while getting interface information.");
        exit(EXIT_FAILURE);
    }

    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {

        if (ifa->ifa_addr == NULL){
            continue;
        }

        if(strcmp(ifa->ifa_name, if_name) == 0){
            if(ifa->ifa_addr->sa_family == AF_INET6){
                struct sockaddr_in6 *in6 = (struct sockaddr_in6*) ifa->ifa_addr;
                memcpy(&(addr->sin6_addr), &(in6->sin6_addr), sizeof(addr->sin6_addr));
                return EXIT_SUCCESS;
            }
        }

    }
    return EXIT_FAILURE;
}

int send_pong(struct sockaddr_in6 * address, int socket_desc, int count){
    int s, addr_len = sizeof(*address);
    char message[1024];
    int length = snprintf( NULL, 0, "%d", count) + 1;

    if(length > 1020){
        printf("Max count of pingpong. No more pongs can be sent.\n");
        return EXIT_FAILURE;
    }

    char count_char[length];
    snprintf(count_char, length, "%d", count);
    strncpy(message, ANSWER, strlen(ANSWER));
    strncpy(&message[strlen(ANSWER)], count_char, length);

    if((s = sendto(socket_desc, message, strlen(ANSWER) + length, MSG_CONFIRM,
                    (const struct sockaddr *) address, sizeof(*address))) < 0){
        printf("Error while sending\n");
        perror("sendto");
        return EXIT_FAILURE;
    }

    printf("Sent %s\n", message);
    return EXIT_SUCCESS;
}

int main(int argc, char** argv) {

    int socket_desc;
    struct sockaddr_in6 servaddr, recv_addr;
    int s, i, slen = sizeof(recv_addr);

    socket_desc = socket(AF_INET6 , SOCK_DGRAM , IPPROTO_UDP);
    if (socket_desc <= 0){
      printf("Could not create socket\n");
    }

    if(s = get_ipv6_from_if(IFNAME, &servaddr) == EXIT_FAILURE){
        printf("Interface tap0 or its IPv6 address was not found.");
        return EXIT_FAILURE;
    }
    servaddr.sin6_family = AF_INET6;
    servaddr.sin6_port = htons(PORT);
    servaddr.sin6_scope_id = if_nametoindex(IFNAME);

    if(bind(socket_desc, (const struct sockaddr *)&servaddr, sizeof(servaddr)) == -1){
      printf("Could not bind socket.\n");
      perror("bind");
    }

    int running = 1;

    char buffer[BUFLEN];
    while (running) {
    int n;

      printf("Waiting for incoming PING\n");

      n = recvfrom(socket_desc, (char *)buffer, BUFLEN, 0, ( struct sockaddr *) &recv_addr, &slen);

      if(n >= 4 && strncmp(buffer, REQUEST, 4) == 0){
          int count = atoi(buffer + 4);
          printf("Received PING Number %i\n", count);
          int s;
          recv_addr.sin6_scope_id = if_nametoindex(IFNAME);
          recv_addr.sin6_port = htons(PORT);
          if(s = send_pong(&recv_addr, socket_desc, count) == -1)
              return EXIT_FAILURE;
          }
      buffer[n] = '\0';
    }

    close(socket_desc);

    return EXIT_SUCCESS;
}
